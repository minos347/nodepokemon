const express = require('express');
const axios = require('axios');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');

const app = express();
const hbs = exphbs.create({defaultLayout: 'main'});

app.engine('handlebars', hbs.engine)

app.set('view engine', 'handlebars');

app.use(bodyParser.urlencoded({extended: true}));

app.get('/pokemon/:name', async (req, res) => {
  try {
    const {name} = req.params;
    const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${name}`)
    const pokemon = response.data;
    res.render('pokemon', {pokemon})
  } catch (e) {
    res.status(404).render('error', {
      message: 'no se encontró Pokemón'
    });
  }
})

app.post('/pokemon', (req, res) => {
  const name = req.body.name; // Obtiene el valor del formulario
  res.redirect(`/pokemon/${name}`);
});

app.get('/type/:name', async (req, res) => {
  try {
    const {name} = req.params;
    const response = await axios.get(`https://pokeapi.co/api/v2/type/${name}`);
    const pokemonList = response.data.pokemon;
    const pokemonDetails = await Promise.all(pokemonList.map(async (item) => {
      const pokemonResponse = await axios.get(item.pokemon.url);
      return pokemonResponse.data;
    }));
    res.render('type', {name, pokemonList: pokemonDetails});
  } catch (error) {
    res.status(404).render('error', {
      message: 'No se encontró el tipo de Pokémon'
    });
  }
});

app.post('/type', (req, res) => {
  const name = req.body.name; // Obtiene el valor del formulario
  res.redirect(`/type/${name}`);
});

const port = 3000;

app.listen(port, () => {
  console.log('Mi port ' + port)
})
